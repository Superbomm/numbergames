package be.themehotel.numbergames.commands;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.Command;
import com.eu.habbo.habbohotel.gameclients.GameClient;

import java.util.Random;

public class HighestCommand extends Command {
    public HighestCommand(String permission, String[] keys) {
        super(permission, keys);
    }

    @Override
    public boolean handle(GameClient gameClient, String[] strings) throws Exception {
        if (gameClient.getHabbo().getHabboInfo().getCredits() < 100){
            gameClient.getHabbo().whisper(Emulator.getTexts().getValue("numbergames.cmd_highest.notenough"));
            return true;
        }
        if (strings.length >= 1)
        {
            int gekozenGetal = Integer.parseInt(strings[1]);
            if (gekozenGetal < 100 && gekozenGetal > 0){
                Random random = new Random();
                int getal = random.nextInt(101);
                if (getal > gekozenGetal)
                {
                    gameClient.getHabbo().whisper(Emulator.getTexts().getValue("numbergames.cmd_highest.loser").replace("%number%", String.valueOf(getal)));
                    gameClient.getHabbo().giveCredits(-getal);
                }
                else {
                    gameClient.getHabbo().whisper(Emulator.getTexts().getValue("numbergames.cmd_highest.winner").replace("%number%", String.valueOf(getal)));
                    gameClient.getHabbo().giveCredits(getal);
                }
            } else {
                gameClient.getHabbo().whisper(Emulator.getTexts().getValue("numbergames.cmd_highest.incorrect.usage"));
            }
        } else {
            gameClient.getHabbo().whisper(Emulator.getTexts().getValue("numbergames.cmd_highest.incorrect.usage"));
        }

        return true;
    }
}
