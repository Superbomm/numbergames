package be.themehotel.numbergames;

import be.themehotel.numbergames.commands.HighestCommand;
import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.CommandHandler;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadedEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class NumberGames extends HabboPlugin implements EventListener {
    private static NumberGames INSTANCE = null;

    @Override
    public void onEnable() {
        INSTANCE = this;
        Emulator.getPluginManager().registerEvents(this, this);

        if (Emulator.isReady)
        {
            this.checkDatabase();
        }

        Emulator.getLogging().logStart("[NumberGames] Started NumberGames Plugin!");
    }

    @Override
    public void onDisable() {
        Emulator.getLogging().logShutdownLine("[NumberGames] Stopped NumberGames Plugin!");
    }

    @EventHandler
    public static void onEmulatorLoaded(EmulatorLoadedEvent event){
        INSTANCE.checkDatabase();
    }

    @Override
    public boolean hasPermission(Habbo habbo, String s) {
        return false;
    }

    private void checkDatabase(){
        boolean reloadPermissions = false;

        Emulator.getTexts().register("numbergames.cmd_highest.keys", "hoogst;highest");
        Emulator.getTexts().register("commands.description.cmd_highest", ":highest <number>");
        Emulator.getTexts().register("numbergames.cmd_highest.incorrect.usage", "Choose a positive number below 100!");
        Emulator.getTexts().register("numbergames.cmd_highest.notenough", "You need at least 100 Credits to start this game!");
        Emulator.getTexts().register("numbergames.cmd_highest.loser", "Aww, you lose! My number was %number%, so you have lost %number% Credits!");
        Emulator.getTexts().register("numbergames.cmd_highest.winner", "We have a winner! My number was %number%, so you get %number% Credits!");
        reloadPermissions = this.registerPermission("cmd_highest", "'0', '1', '2'", "1", reloadPermissions);

        if (reloadPermissions)
        {
            Emulator.getGameEnvironment().getPermissionsManager().reload();
        }

        CommandHandler.addCommand(new HighestCommand("cmd_highest", Emulator.getTexts().getValue("numbergames.cmd_highest.keys").split(";")));
    }

    private boolean registerPermission(String name, String options, String defaultValue, boolean defaultReturn)
    {
        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection())
        {
            try (PreparedStatement statement = connection.prepareStatement("ALTER TABLE  `permissions` ADD  `" + name +"` ENUM(  " + options + " ) NOT NULL DEFAULT  '" + defaultValue + "'"))
            {
                statement.execute();
                return true;
            }
        }
        catch (SQLException e)
        {}

        return defaultReturn;
    }

    public static void main(String[] args)
    {
        System.out.println("Don't run this seperately.");
    }
}
